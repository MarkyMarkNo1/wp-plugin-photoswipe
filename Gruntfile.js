'use strict';

module.exports = function (grunt) {

  // Configurable paths
  var config = {
    app: 'photoswipe-foundation',
    src: 'src',
    tmp: '.tmp',
    lib: 'lib',
    dist: 'dist'
  };

  // Display the elapsed execution time of grunt tasks
  // https://www.npmjs.com/package/time-grunt
  require('time-grunt')(grunt);

  // JIT plugin loader for Grunt
  // https://www.npmjs.com/package/jit-grunt
  require('jit-grunt')(grunt);

  // Define the configuration for all the tasks
  grunt.initConfig({

    // Project settings
    config: config,

    // Grunt Task for Installing Bower Dependencies
    // https://npmjs.org/package/grunt-bower-install-simple
    'bower-install-simple': {
      all: {}
    },

    // Validate files with ESLint
    // https://www.npmjs.com/package/grunt-eslint
    eslint: {
      all: [
        'Gruntfile.js',
        '<%= config.src %>/scripts/**/*.js'
      ]
    },

    // Clean files and folders
    // https://www.npmjs.com/package/grunt-contrib-clean
    clean: {
      tmp: {
        files: [{
          dot: true,
          src: [
            '<%= config.tmp %>'
          ]
        }]
      },
      dist: {
        files: [{
          dot: true,
          src: [
            '<%= config.dist %>/*',
            '!<%= config.dist %>/.git*'
          ]
        }]
      }
    },

    // Run predefined tasks whenever watched file patterns are added, changed or
    // deleted
    // https://www.npmjs.com/package/grunt-contrib-watch
    watch: {
      js: {
        files: [
          'Gruntfile.js',
          '<%= config.src %>/scripts/**/*.js'
        ],
        tasks: ['newer:eslint']
      }
    },

    // Copy files and folders
    // https://www.npmjs.com/package/grunt-contrib-copy
    copy: {
      lib: {
        files: [{
          expand: true,
          dot: true,
          cwd: 'bower_components/photoswipe/dist',
          dest: '<%= config.tmp %>/<%= config.app %>/<%= config.lib %>/photoswipe',
          src: [ '**' ]
        }]
      },
      src: {
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= config.src %>',
          dest: '<%= config.tmp %>/<%= config.app %>',
          src: [ '**' ]
        }]
      },
      'use-min-js': {
        options: {
          process: function (content, srcpath) {
            return content.replace(/.js/g, '.min.js');
          }
        },
        files: [{
          expand: true,
          cwd: '<%= config.tmp %>/<%= config.app %>',
          src: [ '<%= config.app %>.php' ],
          dest: '<%= config.tmp %>/<%= config.app %>'
        }]
      }
    },

    // Minify files with UglifyJS
    // https://www.npmjs.org/package/grunt-contrib-uglify
    uglify: {
      options: {
        compress: {
          drop_console: true
        }
      },
      dist: {
        files: {
          '<%= config.tmp %>/<%= config.app %>/scripts/photoswipe-init.min.js': '<%= config.src %>/scripts/photoswipe-init.js'
        }
      }
    },

    // Minify HTML
    // https://www.npmjs.com/package/grunt-contrib-htmlmin
    htmlmin: {
      pswp: {
        options: {
          collapseBooleanAttributes: true,
          collapseWhitespace: true,
          // conservativeCollapse: true,
          removeAttributeQuotes: true,
          removeComments: true,
          removeCommentsFromCDATA: true,
          removeEmptyAttributes: true,
          removeOptionalTags: true,
          // true would impact styles with attribute selectors
          removeRedundantAttributes: false,
          useShortDoctype: true
        },
        files: [{
          expand: true,
          cwd: '<%= config.tmp %>/<%= config.app %>',
          src: [ 'inc/pswp.php' ],
          dest: '<%= config.tmp %>/<%= config.app %>'
        }]
      }
    },

    // Compress files and folders
    // https://www.npmjs.org/package/grunt-contrib-compress
    compress: {
      dist: {
        options: {
          archive: '<%= config.dist %>/<%= config.app %>.zip',
          mode: 'zip'
        },
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= config.tmp %>',
          src: [
            '**',
            '!<%= config.app %>/lib/*/**'
          ],
          dest: ''
        }]
      }
    },

    // Run grunt tasks concurrently
    // https://www.npmjs.com/package/grunt-concurrent
    concurrent: {
      min: [
        'htmlmin',
        'uglify',
        'copy:use-min-js'
      ]
    },

    // Automatic desktop notifications for Grunt errors and warnings
    // https://www.npmjs.com/package/grunt-notify
    notify: {
      build: {
        options: {
          title: 'Build successful',
          message: '\'build\' task done, without errors.'
        }
      }
    }

  });


  grunt.registerTask('default', [
    'build'
  ]);

  grunt.registerTask('serve', [
    'clean:tmp',
    'bower-install-simple',
    'watch'
  ]);

  grunt.registerTask('build', [
    'newer:eslint',
    'clean',
    'bower-install-simple',
    'copy:src',
    'copy:lib',
    'concurrent:min',
    'compress',
    'notify:build'
  ]);

};
