/**
 * Enables PhotoSwipe by Dmitry Semenov.
 *
 * http://photoswipe.com/
 * http://dimsemenov.com/
 *
 */

if (window.jQuery) jQuery(function($) {

  console.log('photoswipe-init.js');

  var gallery,
   pswpElement = $('#pswp').get(0),                                 // Root element of PhotoSwipe
   settings = {
    // history: false,                                                 // [boolean true] History module (back button to close gallery, unique URL for each slide)
    shareEl: false                                                  // [boolean true] Share buttons
   };

  /*
   * Check if a link actually leads to a Media File (exclude Attachment Page,
   * Custom URL)
   */
  var isMediaFileLink = function (anchor) {

    var $anchor = $(anchor);

    /* At least check if href in fact ends with .bmp, .jpg, .png, .webp etc.
       http://stackoverflow.com/a/16002463 */
    return $anchor.attr('href').toLowerCase().match(/\.(bmp|gif|jpg|jpeg|png|svg|webp)$/);

  };

  /*
   * Generate slides array
   */
  var getSlides = function (anchors) {

    var slides = [],
      $anchors = $(anchors);

    $anchors.each(function(){

      var $a = $(this),
        $img = $a.find('img');

      slides.push({

        src:    $a.attr('href'),

        /*
         * #ToDo We need width & height of the full size image here!
         */
        w:      parseInt($img.attr('width'), 10),
        h:      parseInt($img.attr('height'), 10),

        /*
         * small image placeholder, main (large) image loads on top of it, if you skip
         * this parameter - grey rectangle will be displayed, try to define this
         * property only when small image was loaded before
         */
        msrc:   $img.attr('src'),

        title:  $a.siblings('.wp-caption-text').text()

      });

    });

    return slides;

  };

  /*
   * Utilizing .post a[rel~="attachment"] as onclick-selector should work for
   * images with and without caption.
   *
   * Link might lead to Media File, Attachment Page or Custom URL, though.
   */
  $('body').on('click', '.post a[rel~="attachment"]', function (e) {

    console.log('click on .post a[rel~="attachment"]');

    var options = settings,
      slides,
      $this,
      $post,
      $anchors;

    if (!isMediaFileLink(this)) {
      return;
    }

    e.preventDefault();

    $this= $(this);
    $post = $this.closest('.post');
    $anchors = $post.find('a[rel~="attachment"]').filter(function(){
      return isMediaFileLink(this);
    });

    options.galleryUID = parseInt($post.attr('id').replace('post-', ''), 10);   // [integer 1] Used by History module when forming URL
    options.index = $anchors.index($this);                        // [integer 0] Start slide index

    slides = getSlides($anchors.get());

    console.log('options:');
    console.log(options);
    console.log('slides:');
    console.log(slides);

    gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, slides, options);
    gallery.init();

  });

});
