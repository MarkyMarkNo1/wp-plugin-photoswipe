===========================================
http://photoswipe.com/ - License and price:
===========================================

  "The script is free to use for personal and commercial projects. It falls under
  the MIT license with one exception: Do not create a public WordPress plugin
  based on it, as I will develop it."


* Download PhotoSwipe from https://github.com/dimsemenov/photoswipe
* Put its 'dist' folder right here,
* but rename it to 'photoswipe'.


Enjoy!
