<?php
/**
 * Plugin Name: PhotoSwipe Foundation
 * Plugin URI: https://bitbucket.org/MarkyMarkNo1/wp-plugin-photoswipe
 * Description: Enables <a href="http://photoswipe.com/">PhotoSwipe</a> by <a href="http://dimsemenov.com/">Dmitry Semenov</a>.
 * Version: 0.1.0-develop
 * Author: MarkyMarkNo1
 * Author URI: https://bitbucket.org/MarkyMarkNo1
 * License: GPL2
 */


/*
 * http://photoswipe.com/documentation/getting-started.html
 */


/*
 * Step 1: include JS and CSS files
 */
function photoswipe_foundation_enqueue_assets () {

  /* Core CSS file */
  wp_enqueue_style( 'photoswipe', plugin_dir_url( __FILE__ ) . 'lib/photoswipe/photoswipe.css' );
  /* Skin CSS file (styling of UI - buttons, caption, etc.) */
  wp_enqueue_style( 'photoswipe-default-skin', plugin_dir_url( __FILE__ ) . 'lib/photoswipe/default-skin/default-skin.css' );

  /*
   * wp_enqueue_script( $handle, $src, $deps, $ver, $in_footer )
   *
   * https://codex.wordpress.org/Function_Reference/wp_enqueue_script
   */

  /* Core JS file */
  wp_enqueue_script( 'photoswipe-core', plugin_dir_url( __FILE__ ) . 'lib/photoswipe/photoswipe.js', array(), '', true );
  /* UI JS file */
  wp_enqueue_script( 'photoswipe-ui', plugin_dir_url( __FILE__ ) . 'lib/photoswipe/photoswipe-ui-default.js', array( 'photoswipe-core' ), '', true );

  /* Step 3: initialize */
  wp_enqueue_script( 'photoswipe-init', plugin_dir_url( __FILE__ ) . 'scripts/photoswipe-init.js', array( 'jquery', 'photoswipe-ui' ), '', true );
}
add_action( 'wp_enqueue_scripts', 'photoswipe_foundation_enqueue_assets' );



/*
 * Step 2: add PhotoSwipe (.pswp) element to DOM
 */
function photoswipe_foundation_append_pswp () {
?>
<!-- photoswipe_foundation_append_pswp -->
<?php require 'inc/pswp.php'; ?>
<!-- end photoswipe_foundation_append_pswp -->
<?php
}
add_action( 'wp_footer', 'photoswipe_foundation_append_pswp' );



