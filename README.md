# PhotoSwipe Foundation plugin for WordPress #

Enables [PhotoSwipe](http://photoswipe.com/) by [Dmitry Semenov](http://dimsemenov.com/).

## About WordPress ##

* [WordPress Codex](http://codex.wordpress.org/Writing_a_Plugin)
* [WordPress.org](http://wordpress.org/)
